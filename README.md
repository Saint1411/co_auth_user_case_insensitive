>This module makes user logins case insensitive. It also overwrites the search method to allow these case insensitive logins to work on a database that previously had case sensitive logins.

**Author:** _ERP CloudMedia_

**Contributors:**
- [Cloud Media](https://cloudmedia.vn)
- [Dave Lasley](dave@laslabs.com)
- [Ted Salmon](tsalmon@laslabs.com)
- [Mayank Gosai](mgosai@opensourceintegrators.com)
- [Chandresh Thakkar](chandresh.thakkar.serpentcs@gmail.com)
