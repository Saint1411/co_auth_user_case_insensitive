# -*- coding: utf-8 -*-
{
    'name': "Co Case Insensitive Logins",

    "summary": """Makes the user login field case insensitive""",

    "description": """
        Makes the user login field case insensitive
    """,

    "author": "Cloud Media",
    "website": "https://cloudmedia.vn",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/14.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    "category": "Authentication",
    "version": "14.0.1.0.1",

    # any module necessary for this one to work correctly
    "depends": ['base'],

    # Add the following to the list of funtion in hooks.py
    "pre_init_hook": "pre_init_hook_login_check",
    "post_init_hook": "post_init_hook_login_convert",

    "application": False,
    "installable": True,
}
